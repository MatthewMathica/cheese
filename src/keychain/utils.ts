// import bip39 from 'bip39'
// import { keys, } from '@libp2p/crypto'
// import * as crypto from 'crypto'
export const Crypto = 'undefined'
// export const Crypto = {

//     randomString(numOfBytes: any) {
//         return crypto.randomBytes(numOfBytes).toString('hex');
//     },

//     sha256(data: any) {
//         return crypto.createHash('sha256').update(data).digest('hex');
//     },

//     generatePhrase() {
//         return bip39.generateMnemonic();
//     },

//     phraseToSeed(phraseArray: string[]) {
//         return bip39.mnemonicToSeedSync(phraseArray.join(' '));
//     },

//     validatePhrase(phraseArray: string[]) {
//         return bip39.validateMnemonic(phraseArray.join(' '));
//     },

//     async importPhrase(phrase: string[], passphrase: string) {

//         const seed = this.phraseToSeed(phrase);
//         const privKey = crypto.createHash('sha256').update(seed).digest();
        
//         const pair = await keys.generateKeyPairFromSeed('Ed25519', privKey, 2048);

//         return pair.export(passphrase, 'libp2p-key');
//     },

//     async decryptKey(key: string, passphrase: string) {
//         const pkey = await keys.importKey(key, passphrase);
//         return keys.marshalPrivateKey(pkey, 'Ed25519');
//     }
// };
