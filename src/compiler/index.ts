enum SharedType {
  CRDTArray,
  CRDTMap,
  CRDTText,
  CRDTNumber,
  DAGArray,
  DAGMap,
}

enum Permission {
  CAN_APPEND,
  CAN_CHANGE,
  CAN_DELETE
}

interface PermissionPattern {
  peerId: string
  permission: Permission
}

interface TypedPattern {
  type: SharedType
  permissions: PermissionPattern[]
  value?: TypedPattern | TypedPattern[] | Map<string, TypedPattern | any>
}

interface AbstractDocument {
  [key: string]: TypedPattern
}

export class DocumentCompiler {
  constructor() {}
  compile(json: object): AbstractDocument {
    return {}
  }
}

const document = {
  msgs: <TypedPattern>{
    type: SharedType.CRDTArray,
    permissions: [{
      peerId: 'a', permission: Permission.CAN_APPEND
    }],
    value: <TypedPattern>{}
  }
}