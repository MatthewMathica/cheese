// @ts-nocheck
import {
  deserialize,
  serialize,
  field,
  variant,
  vec,
  option
} from "@dao-xyz/borsh";

abstract class AbstractDocument {
  test() { return 'ABSTRACT' }
}

enum PermissionType {
  CAN_CHANGE,
  CAN_APPEND,
  CAN_DELETE
}

enum CRDTSharedType {
  ARRAY,
  MAP,
  TEXT,
  NUMBER
}

enum DAGSharedType {
  ARRAY,
  MAP,
  TEXT,
}

@variant('permission')
class Permission {
  @field({ type: 'string'})
  public peerId: string;
  @field({ type: PermissionType })
  public permission: PermissionType
}

@variant(0)
class CRDT extends AbstractDocument {
  @field({ type: 'u8'})
  public type: CRDTSharedType

  constructor() {
    super()
    this.type = CRDTSharedType.MAP
  }

  test() {
    return 'CRDT'
  }
}

@variant(1)
class DAG extends AbstractDocument {
  @field({ type: 'u8'})
  public type: DAGSharedType
  constructor() {
    super()
    this.type = DAGSharedType.MAP
  }

  test() {
    return 'DAG'
  }
}

@variant(2)
class Room extends AbstractDocument {
  @field({ type: CRDT })
  public msgs: CRDT
}

function unseal(data: Uint8Array) {
  return deserialize(data, AbstractDocument)
}

function seal(data: any) {
  return serialize(data); 
}

export { seal, unseal, CRDT, DAG, AbstractDocument }
