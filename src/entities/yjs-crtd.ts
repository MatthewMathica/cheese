import Entity, { UpdateResult } from "../index.js";
import * as Y from 'yjs';

export class YjsCrdt extends Entity {

    private internalCrdt: any = null
    
    state(): Uint8Array {
        return Y.encodeStateVector(this.internalCrdt);
    }

    difference(state: Uint8Array): Uint8Array {
        return Y.encodeStateAsUpdate(this.internalCrdt, state);
    }

    sync(peerId: any, update: Uint8Array): UpdateResult {
        Y.applyUpdate(this.internalCrdt, update, this)
        return UpdateResult.SUCCESS
    }

    generateUUID(): string {
        throw new Error("Method not implemented.");
    }

    render() {
        throw new Error("Method not implemented.");
    }

    onUpdate(update: Uint8Array, origin: any) {
        if (origin == null) {
            this.emit('update', {
                documentId: this.internalCrdt.guid,
                stateVector: this.state(),
                stateUpdate: update
            });
        } else {
            this.emit('changed', true);
        }
    }

    constructor(doc: any) {
        super(doc.guid);
        this.internalCrdt = doc
        doc.on('update', this.onUpdate.bind(this));
    }


}