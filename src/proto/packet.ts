/* eslint-disable import/export */
/* eslint-disable complexity */
/* eslint-disable @typescript-eslint/no-namespace */
/* eslint-disable @typescript-eslint/no-unnecessary-boolean-literal-compare */

import { encodeMessage, decodeMessage, message } from 'protons-runtime'
import type { Uint8ArrayList } from 'uint8arraylist'
import type { Codec } from 'protons-runtime'

export interface BulletSync {
  stateVector: Uint8Array
  stateUpdate: Uint8Array
}

export namespace BulletSync {
  let _codec: Codec<BulletSync>

  export const codec = (): Codec<BulletSync> => {
    if (_codec == null) {
      _codec = message<BulletSync>((obj, w, opts = {}) => {
        if (opts.lengthDelimited !== false) {
          w.fork()
        }

        if (opts.writeDefaults === true || (obj.stateVector != null && obj.stateVector.byteLength > 0)) {
          w.uint32(10)
          w.bytes(obj.stateVector)
        }

        if (opts.writeDefaults === true || (obj.stateUpdate != null && obj.stateUpdate.byteLength > 0)) {
          w.uint32(18)
          w.bytes(obj.stateUpdate)
        }

        if (opts.lengthDelimited !== false) {
          w.ldelim()
        }
      }, (reader, length) => {
        const obj: any = {
          stateVector: new Uint8Array(0),
          stateUpdate: new Uint8Array(0)
        }

        const end = length == null ? reader.len : reader.pos + length

        while (reader.pos < end) {
          const tag = reader.uint32()

          switch (tag >>> 3) {
            case 1:
              obj.stateVector = reader.bytes()
              break
            case 2:
              obj.stateUpdate = reader.bytes()
              break
            default:
              reader.skipType(tag & 7)
              break
          }
        }

        return obj
      })
    }

    return _codec
  }

  export const encode = (obj: BulletSync): Uint8Array => {
    return encodeMessage(obj, BulletSync.codec())
  }

  export const decode = (buf: Uint8Array | Uint8ArrayList): BulletSync => {
    return decodeMessage(buf, BulletSync.codec())
  }
}
