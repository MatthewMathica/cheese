// Importing dependencies
import { BulletSync } from './proto/packet.js';
import { pushable } from 'it-pushable';
import { areEqualByteArrays } from './utils.js';
import type { Libp2p } from 'libp2p'
import { peerIdFromString } from '@libp2p/peer-id'

// Importing types
import type Entity from './index';
import type { Stream } from '@libp2p/interface-connection'
import type { IncomingStreamData } from '@libp2p/interface-registrar'

class Sync {

    // Class properties
    static protocolId: string = 'cheese';
    static version = '1.0.0';
    static _name:string = 'syncer'

    // Instance properties
    _peers: Map<string, boolean>;
    _libp2p: Libp2p<any> = undefined as unknown as Libp2p<any>;
    public _document: Entity;
    isPublic: boolean;

    // Constructor
    constructor(document: Entity, isPublic = true) {
        this._document = document;
        this.isPublic = isPublic;
        this._peers = new Map();
    }

    // Method for adding peers
    addPeer(peerId: any) {
        if (!this._peers.get(peerId)) {
            this._peers.set(peerId, true);
        }
    }

    // Getter for protocol name
    get protocolName () {
        return `${Sync._name}/${this._document.uuid}/${Sync.version}`;
    }

    // Start method for initiating the Sync instance
 
    async start(libp2p: Libp2p<any>) {
        this._libp2p = libp2p;
        this._libp2p.handle(this.protocolName, this.handle.bind(this));
        this._libp2p.services.pubsub.subscribe(this.protocolName);
        this._libp2p.services.pubsub.addEventListener(
            'message',
            (evt: any) => {
                //@ts-ignore
                const { topic, data, from  } = evt.detail
                if (topic !== this.protocolName) return 
                const {stateVector, stateUpdate} = BulletSync.decode(data);
                if (!areEqualByteArrays(stateVector, this._document.state())) {
                    this.fetch(from.toString());
                } else {
                   this._document.sync(from, stateUpdate);
                }
            }
        );

        this._document.on('update', ({stateVector, stateUpdate}) => {
            const data = BulletSync.encode({
                stateVector,
                stateUpdate
            });
            this._libp2p.services.pubsub.publish(this.protocolName, data);
        });
    }

    // Stop method for terminating the Sync instance
    async stop() {
        this._libp2p.services.pubsub.unsubscribe(this.protocolName);
        this._libp2p.services.pubsub.removeEventListener(this.protocolName as any);
        this._libp2p.unhandle(this.protocolName);
        this._libp2p.stop();
    }

    // Static method for reading from a stream
    static async read(stream: Stream) {
        const { value: chunk } = await (stream.source as AsyncIterable<any>)[Symbol.asyncIterator]().next();
        return chunk.slice();
    }

    // Method for fetching from a peer
    async fetch(peerIdString: string) {
        // Convert the peerId string to a PeerId object
        const peerId: any = peerIdFromString(peerIdString);
        // Create a new stream by dialing to the peer using the protocol name
        const stream  = await this._libp2p.dialProtocol(peerId, [this.protocolName]);
        const writable = pushable();
        stream.sink(writable);
        // Write the current document state to the stream
        writable.push(this._document.state());
        // Read the peer's state vector from the stream
        const stateVector = await Sync.read(stream);
        // Write the difference between the document states to the stream
        writable.push(this._document.difference(stateVector));
        // Read the peer's state update from the stream
        const stateUpdate = await Sync.read(stream);
        // Sync the local document with the state update received from the peer
        this._document.sync(peerIdString, stateUpdate);
        // End writing to the stream
        writable.end();
    }

    // Method to handle incoming data from a stream
    async handle(data: IncomingStreamData) {
        const { stream, connection } = data;
        const peerId = connection.remotePeer.toString();
        const writable = pushable();
        stream.sink(writable);
        // Read the peer's state vector from the stream
        const stateVector = await Sync.read(stream);
        // Write the current document state to the stream
        writable.push(this._document.state());
        // Read the peer's state update from the stream
        const stateUpdate = await Sync.read(stream);
        // Sync the local document with the state update received from the peer
        this._document.sync(peerId, stateUpdate);
        // Write the difference between the document states to the stream
        writable.push(this._document.difference(stateVector));
        // End writing to the stream
        writable.end();
    }
}

// Export the Sync class
export { Sync };