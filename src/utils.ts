export function areEqualByteArrays(arr1: Uint8Array, arr2: Uint8Array) {
    for (let c = 0; c < arr1.length; c++) {
        if (arr1[c] != arr2[c]) return false
    }
    return true
}