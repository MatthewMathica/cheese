import Entity, { UpdateResult } from "../index.js";
// import { Fireproof } from '@fireproof/core'

export class ProllyDag extends Entity {

    private internalStructure: any = null;

    state(): Uint8Array {
        throw new Error("Method not implemented.");
    }
    difference(state: Uint8Array): Uint8Array {
        throw new Error("Method not implemented.");
    }
    sync(peerId: any, update: Uint8Array): UpdateResult {
        throw new Error("Method not implemented.");
    }
    generateUUID(): string {
        throw new Error("Method not implemented.");
    }
    render() {
        return this.internalStructure
    }

    constructor(doc: any) {
        super(doc.id);
        this.internalStructure = doc
    }

}