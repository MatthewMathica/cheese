import { EventEmitter } from "events"

export * from './protocol.js';
export * from './keychain/utils.js'
export enum UpdateResult {
    SUCCESS,
    ERROR,
    ILLEGAL
}

export type Update = Uint8Array
export type State = Uint8Array

/**
 * Psy-doc is object that syncs all of its property values between peers
 */
export default abstract class Entity extends EventEmitter {
    public readonly uuid: string
    constructor (uuid?: string) {
        super()
        this.uuid = uuid ?? this.generateUUID()
    }

    // Get current state description of object (think of it like version number)
    abstract state(): State
    // Calculates missing updates for remote state description
    abstract difference(state: State): Update
    // It tries to apply incoming update
    abstract sync(peerId: any, update: Update): UpdateResult
    // Generate uuid
    abstract generateUUID(): string
    // It renders current object state
    abstract render(): any
}
