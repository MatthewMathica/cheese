export enum RuleResult {
    DENIED,
    APPROVED
} 

export enum RuleError {
    INSUFFICIENT_PRIVILEGES,
    UNAUTHORIZED
}

const documentRuleParams = ['document', 'peer', 'data']

export class Rules {

    private fnc: Function
    constructor(code: string) {
        this.fnc = new Function(...documentRuleParams, code)
    }

    handle(...args: any[]): RuleResult {
        return this.fnc(...args)
    }
}