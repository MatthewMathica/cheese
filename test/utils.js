import { createLibp2p } from 'libp2p'
import { tcp } from '@libp2p/tcp'
import { mplex } from '@libp2p/mplex'
import { mdns } from '@libp2p/mdns'
import { noise } from "@chainsafe/libp2p-noise";
import { gossipsub } from '@chainsafe/libp2p-gossipsub';
import { identifyService } from 'libp2p/identify'

export const config = {
    addresses: {
        listen: ['/ip4/0.0.0.0/tcp/0']
    },
    transports: [tcp()],
    streamMuxers: [mplex()],
    connectionEncryption: [noise()],
    peerDiscovery: [mdns()],
    services: {
        pubsub: gossipsub(),
        identify: identifyService()
    }
};

export function wait(time = 1000) {
    return new Promise(res => setTimeout(res, time));
}

// @ts-ignore
export function arrayEquals(a, b) {

    if (a.length != b.length) return false;

    for (let i = 0; i < a.length; i++) {
        if (a[i] != b[i]) return false;
    }

    return true;
}

// @ts-ignore
export function waitTill(fnb, timeout = 1000) {
    const dT = 40;
    let t = 0;
    return new Promise(async (res, err) => {
        while(!fnb()) {
            await wait(dT);
            t += dT;
            if (t >= timeout) {
                err('Timeout exceeded');
            }
        }
        // @ts-ignore
        res();
    });
}

export async function createNode() {
    // @ts-ignore
    const node = await createLibp2p(config);
    await node.start()
    return node;
}

// @ts-ignore
async function connectNodes(nodes) {
    for (let i = 0; i < nodes.length; i++) {
        const f = nodes[i];
        for (let j = i + 1; j < nodes.length; j++) {
            const s = nodes[j];
            await f.peerStore.patch(s.peerId, {
                multiaddrs: s.getMultiaddrs()
              })
            await f.dial(s.peerId)
        }
    }

    return nodes;
}

// @ts-ignore
export async function stopNodes(nodes) {
    // @ts-ignore
    return Promise.all(nodes.map(n => n.stop()));
}

export async function createPeers(n = 2) {
    let peers = [];
    for (let i = 0; i < n; i++) {
        peers.push(createNode());
    }
    const res = await Promise.all(peers)
    try {
        await connectNodes(res)
    } catch (e) {
        console.log(e)
    }
    return res
}