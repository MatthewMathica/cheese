import { Rules, RuleResult } from '../src/rules.js'
import { expect } from 'chai'
import * as Y from 'yjs'
import { Sync } from '../src/protocol.js'
import { YjsCrdt } from '../src/entities/yjs-crtd.js'
import { createPeers, wait, stopNodes } from './utils.js'
import { CRDT, DAG, seal, unseal } from '../src/recipe.js'

describe('Recipes', function() {

    it.only('Seal and Unseal document recipe', () => {
        const crdt = [
            new CRDT(),
            new DAG()
        ]
        
        const serialized = crdt.map(instance => seal(instance))
        const deserialized = serialized.map(binary => unseal(binary))
        expect(deserialized.map(instance => instance.test())).eql(['CRDT', 'DAG'])
    })
})

describe('Rules', function () {

    let doc = {
        msgs: [1]
    }

    it('Assigns value and returns SUCCESS', () => {
        const editorCode = `
            document.msgs[0] = 15;
            return 1;
        `;

        const rules = new Rules(editorCode)
        const result = rules.handle(doc)
        expect(result).to.be.eql(RuleResult.APPROVED)
        expect(doc.msgs[0]).to.be.eql(15)
    })
    it('Evaluates action with respect to permissions', () => {

        const editorCode = `
            if (peer !== 'Reflex') {
                return 0
            }
            return 1;
        `
        const rules = new Rules(editorCode)
        const [success, denied] = [
            rules.handle(doc, 'Reflex'),
            rules.handle(doc, 'Gabaj')
        ]
        expect(success).to.be.eql(RuleResult.APPROVED)
        expect(denied).to.be.eql(RuleResult.DENIED)

    })
})

describe('Sync protocol', function () {

    // @ts-ignore
    let documents = undefined;
    // @ts-ignore
    let syncer = undefined;
    // @ts-ignore
    let peers = undefined;

    before(async function () {
        documents = [
            // @ts-ignore
            new Y.Doc({guid: 1}),
            // @ts-ignore
            new Y.Doc({guid: 1})
            // @ts-ignore
        ].map(doc => new Sync(new YjsCrdt(doc)));

        // @ts-ignore
        peers = await createPeers(2);
        for (let i = 0; i < peers.length; i++) {
            await documents[i].start(peers[i])
        }
    });

    after(async function () {
        // @ts-ignore
        await stopNodes(peers);
    });

    it('magazine sync', async function() {

        // @ts-ignore
        const [A, B] = documents;
        A._document.internalCrdt.getArray('messages').insert(0, ['ok2']);
        B._document.internalCrdt.getArray('messages').insert(0, ['dobre2']);

        await B.fetch(A._libp2p.peerId.toString());
        expect(A._document.internalCrdt.getArray('messages').toArray())
            .to.be.eql(B._document.internalCrdt.getArray('messages').toArray());
    })


    it('bullet sync', async function() {

        // @ts-ignore
        const [n1, n2] = peers;
        // @ts-ignore
        const [A, B] = documents;

        // @ts-ignore
        await wait(100);

        A._document.internalCrdt.getArray('messages').insert(0, ['ok']);
        await wait(100);

        B._document.internalCrdt.getArray('messages').insert(0, ['dobre']);

        // @ts-ignore
        await wait(100);

        expect(A._document.internalCrdt.getArray('messages').toArray())
            .to.be.eql(B._document.internalCrdt.getArray('messages').toArray());
    });

});
